B"H

# This is my coupon project.
author @Shmuel Raichman

The project consist of 
* BE - BackEnd 
* FE - FrontEnd <br>
 
Both are currently served by the same spring web server.

BE - written in java with spring boot framework. <br>
FE - an altered Bootstrap template with jquery.

The site and API are available at [My Site](http://35.243.185.205:8080)  

To run the project on your machine :

Currently the code is pointing to my own db (in cloud) and should work without any db configuration on any PC.

If the you want to use your own db or the cloud db not working for you you will need.<br>

Mysql server installed.<br>
Schema named coupons in your my sql server.<br>
Change jest the ip address of the property spring.datasource.url in the application.properties file to localhost.<br>
The db login is now user: root password: root so if you use your own db you may need to change it.<br>
The tables should be generated automatically or it's use the existing one if exist.<br>

There is another file in this repo with the api details [API.md](./API.md). <br>
If you using post man i added a postman collection as json you can import with all the api (i hope its all.) [coupon project](./coupons project.postman_collection.json).

The project is missing the login functionality witch 
means the full api is accessible through specific roll controller for everyone.<br> 

Currently there is only company page in FE. (hopefully there will be page for all rolls until i send it.) <br>
also i didn't got to do the income service.

I know i'm missing some parts but i did my best and i hope it's will be enough to pass this course.

Thank you Ameed for the course it's was grate i learn a lot and i'm using it in my current job.