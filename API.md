B"H

# This is my coupons project API.

All api requests starts with /rest/api

---
Admin Facade - 
Requests at /admin
----------------------------------------------------------------------
Create company. 
```
METHOD: POST 
Path: /createcompany
```

@RequestBody
```json

{
	"compName": "name",
	"password": "password",
	"email": "email@mail.com"
}
```
```java
/**
* @Params Company(company name, company password, company email)
* @Return Company
*/
```

---
Update company email.
```
METHOD: POST 
Path: /updatecompanyemail
```

@RequestBody
```json
{
    "id": 71,
    "email": "comp-7"
}
```
```java
/**
* @Params Company(CompanyID, new email) 
* @Return Company
*/
```

---
Update company password.
```
METHOD: POST 
Path: /updatecompanypassword
```

@RequestBody
```json
{
    "id": 7,
    "password": "comp-7"
}
```
```java
/**
* @Params Company(CompanyID, new password)
* @Return Company
*/
```

---
Get all companies.
```
METHOD: GET 
Path: /getAllCompanies
```

```java
/**
* @Params none 
* @Return List<Company>
*/
```

---
Get company by id.
```
METHOD: GET 
Path: /getCompanyById/{id}
```

```java
/**
* @Params companyID 
* @Return Company
*/
```

---
Add new customer.
```
METHOD: POST
Path: /addNewCustomer
```

@RequestBody
```json
{
    "custName": "Tom Rio",
    "password": "Aa123456",
    "email": "tom@mail.com"
}
```
```java
/**
* @Params Customer(Customer name, Customer password, Customer email) 
* @Return Customer
*/
```

---
Get customer by id.
```
METHOD: GET
Path: /getCustomerByid/{id}
```

```java
/**
* @Params customerID 
* @Return Customer
*/
```

---
Update customer email.
```
METHOD: POST
Path: /updatecustomeremail
```

@RequestBody
```json
{
    "id": 71,
    "email": "comp-7"
}
```
```java
/**
* @Params Customer(CustomerID, new email) 
* @Return Customer
*/
```

---
Update customer password.
```
METHOD: POST
Path: /updatecustomerpassword
```

@RequestBody
```json
{
    "id": 7,
    "password": "comp-7"
}
```
```java
/**
* @Params Customer(CustomerID, new password) 
* @Return Customer
*/
```

---
Get all customers.
```
METHOD: GET - 
Path: /getAllCustomers
```

```java
/**
* @Params none 
* @Return List<Customer>
*/
```

---
Delete customer by id.
```
METHOD: DELETE 
Path: /deleteCustomerById/{id}
```

```java
/**
* @Params customerID 
* @Return boolean
*/
```
---
Delete company by id.
```
METHOD: DELETE
Path: /deleteCompanyById/{id}
```

```java
/**
* @Params companyID
* @Return boolean
*/
```

---
---
Company Facade - 
Requests at /company
---------------------------------------------------------------------- 
---------------------------------------------------------------------- 
Crete new coupon.
```
METHOD: POST
Path: /createNewCoupon?companyID={id}
```

@RequestBody
```json
{
    "title": "Coupon 55",
    "endDate": "2020-01-06T19:26:56.594Z",
    "amount": 5,
    "message": "Put something in db",
    "price": 80.00,
    "imageLink": "Testing.co.uk",
    "couponCategory": "BABY"
}
```
```java
/**
* @Params Coupon(title, end date, amount, message, price, imageLink, couponCategory[BABY,SPORTS,HEALTH,FOOD]), companyID 
* @Return Coupon
*/
```

---
Delete coupon.
```
METHOD: DELETE 
Path: /deleteCoupon/{id}
```

```java
/**
* @Params couponID 
* @Return boolean
*/
```

---
Update coupon expiration date.
```
METHOD: POST
Path: /updateCouponExpireDate
```

@RequestBody
```json
{
    "id": "61",
    "endDate": "3019-01-06T20:26:31.923Z"
}
```
```java
/**
* @Params Coupon(id, end date) <br>
* @Return Coupon
*/
```
---
Update coupon price.
```
METHOD: POST 
Path: /updateCouponPrice
```

@RequestBody
```json
{
    "id": "61",
    "price": 150
}
```
```java
/** 
* @Params Coupon(id, price) 
* @Return Coupon
*/
```

---
Get all company coupons.
```
METHOD: GET 
Path: /getAllCouponsForCompany/{companyID}
```

```java
/**
* @Params companyID
* @Return Set<Coupon> 
*/
```

---
Get company coupons up to price.
```
METHOD: GET
Path: /getCouponsForCompanyByPrice?companyID={id}&price={price}
```
Note: i know method path give the wrong immersion ... 
```java
/**
* @Params companyID, price 
* @Return Set<Coupon>
*/
```

---
Get company coupons by category.
```
METHOD: GET 
Path: /getCouponsForCompanyByType?companyID={id}&category={category}
```
```java
/**
* @Params companyID, category 
* @Return Set<Coupon>
*/
```

---
Get company coupons that ends before given date.
```
METHOD: GET 
Path: /getCouponsForCompanyByEndDate?companyID={id}&date={end date} 
Example date format: [3018-01-06T20:26:31.923+0000]
```
@RequestBody
```json
{
    "id": 28,
    "endDate": "3018-01-06T20:26:32.000+0000"
}
```
```java
/**
* @Params Coupon(companyID, category)
* @Return Set<Coupon>
*/
```

@smuel1414 This is probably not a very good way to pass the parameters but ... !

---
Get company deciles.
```
METHOD: GET  
Path: /getMyCompany/{companyID}
```
```java
/**
* does not return company coupons.
* @Params companyID
* @Return Company
*/
```

---
---
Customer Facade - Requests at /customer
---
---
Purchase coupon.
Add coupon to customer.
```
METHOD: POST 
Path: /addCouponToCustomer?couponID={id}&customerID={id}
```
```java
/**
* @Params couponID, customerID
* @Return Coupon
*/
```

---
Get customer coupons.
```
METHOD: GET 
Path: /getCouponsHistory/{id}
```
```java
/**
* @Params customerID
* @Return Set<Coupon>
*/
```
---
Get customet coupons up to price.
```
METHOD: GET 
Path: /getCouponUpToPrice?customerID={id}&price={price}
```
```java
/**
* @Params customerID, price
* @Return Set<Coupon>
*/

```

---
Get customer coupons by category.
```
METHOD: GET
Path: /getCouponByType?customerID={id}&category={category}
```

```java
/**
* @Params customerID, price
* @Return Set<category>
*/
```

---
---
ScheduledTasks - Requests at /scheduledTasks
---
---
```
METHOD: GET -
Path: /stopScheduler
```

```java
/**
* @Return String "Ok"
*/
```


---
```
METHOD: GET -
Path: /startScheduler
```

```java
/**
* @Return String "Ok"
*/
```

---
```
METHOD: GET -
Path: /listScheduler
```

```java
/**
* @Return ScheduledTasks list
*/
```
