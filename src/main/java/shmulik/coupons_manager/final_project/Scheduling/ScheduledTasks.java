// B"H
package shmulik.coupons_manager.final_project.Scheduling;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import shmulik.coupons_manager.final_project.entities.Coupon;
import shmulik.coupons_manager.final_project.services.interfaces.CouponService;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

@Component
public class ScheduledTasks {
    private static final Logger logger = LoggerFactory.getLogger(ScheduledTasks.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");
    private static final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy' T-'HH:mm:ss");
    private static final String scheduledTimeEvryMinut = "0 * * * * ?";
    private static final String scheduledTime = "0 0 0 * * ?";


//    @Autowired
//    static ThreadPoolTaskScheduler scheduler;

    @Autowired
    private CouponService couponService;

    @Scheduled(cron = scheduledTime)
    public void scheduleTaskWithCronExpression() {
        logger.info("Cron Task :: Execution Time - {} ", dateFormatter.format(LocalDateTime.now()) );
        List<Coupon> couponList = couponService.findAll();
        couponList.forEach(coupon -> {
            if(coupon.getEndDate().before(new Date())){
                couponService.deleteCouponById(coupon.getId());
            }
        });
        logger.info("Current Thread : {}", Thread.currentThread().getName());
    }
}
